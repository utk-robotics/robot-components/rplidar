#include "rip/components/rplidar.hpp"

// Local
#include "rip/exception_base.hpp"
#include "rip/logger.hpp"
#include <units.h>

//Global
#include <algorithm>
#include <chrono>
#include <ratio>
// TODO: Add factory constructor, and a non json based constructor
// for normal instantiation.
namespace rip::components
{

    using inch_t   = units::length::inch_t;
    using degree_t = units::angle::degree_t;

    RpLidar::RpLidar(const nlohmann::json& config)
        : Lidar()
        , m_driver(nullptr)
    {
        m_driver = RPlidarDriver::CreateDriver(rp::standalone::rplidar::DRIVER_TYPE_SERIALPORT);
        if(!m_driver)
        {
            throw DriverException("Error: creating driver");
        }

        if(config.find("serial_port") != config.end())
        {
            m_serial_port = config.at("serial_port");
        }
        else
        {
            m_serial_port = "/dev/ttyUSB0";
        }

        if(config.find("baud_rate") != config.end())
        {
            m_baud_rate = config.at("baud_rate");
        }
        else
        {
            m_baud_rate = 115200;
        }

        try
        {
            m_scan_size = config["scan_size"].get< int >();
        }
        catch(nlohmann::json::out_of_range& e)
        {
            rip::Logger::error("Lidar Config missing: {}", e.what());
            throw ConfigException("Lidar Config missing: {}", e.what());
        }
    }

    RpLidar::RpLidar(int scan_size, std::string serial_port, int baud_rate)
        : m_serial_port(serial_port)
        , m_baud_rate(baud_rate)
        , m_scan_size(scan_size)
        , m_driver(nullptr)
    {
        m_driver = RPlidarDriver::CreateDriver(rp::standalone::rplidar::DRIVER_TYPE_SERIALPORT);
        if(!m_driver)
        {
            throw DriverException("Error: creating driver");
        }
    }
    
    RpLidar::~RpLidar()
    {
        stop();
        RPlidarDriver::DisposeDriver(m_driver);
    }

    void RpLidar::start()
    {
        if(IS_FAIL(m_driver->connect(m_serial_port.c_str(), (_u32) m_baud_rate)))
        {
            throw ConnectionException("Error: cannot bind to the serial port: {}", m_serial_port);
        }

        m_driver->startMotor();
        m_driver->startScan(false, true);
        m_thread = std::make_unique< std::thread >(&RpLidar::run, this);
    }

    void RpLidar::stop()
    {
        if(m_running)
        {
            m_running = false;
            m_thread->join();
        }

        if(m_driver->isConnected())
        {
            m_driver->stop();
            m_driver->stopMotor();
            m_driver->disconnect();
        }
    }

    void RpLidar::run()
    {
        m_running = true;
        bool first_run = true;

        rplidar_response_measurement_node_t* nodes = new rplidar_response_measurement_node_t[m_scan_size];
        size_t count                               = m_scan_size;
        m_last_scan.resize(360);
        std::vector<LaserScan> scan1;
        scan1.resize(count);
        std::vector<LaserScan> scan2;
        scan2.resize(count);
        while(m_running)
        {
            for (unsigned int i = 0; i < 2; i++)
            {
                // Grab the scan nodes
                u_result op_result = m_driver->grabScanData(nodes, count);
                if (first_run)
                {
                    first_run = false;
                    op_result = m_driver->grabScanData(nodes, count);
                    break;
                }

                if(op_result == RESULT_OK)
                {
                    // Sort the scan nodes (as it does not come sorted)
                    op_result = m_driver->ascendScanData(nodes, count);
                    if(op_result == RESULT_OK)
                    {
                        // Angle compensation
                        const int angle_compensate_nodes_count = 360;
                        const int angle_compensate_multiple    = 1;
                        int angle_compensate_offset            = 0;
                        rplidar_response_measurement_node_t angle_compensate_nodes[angle_compensate_nodes_count];
                        memset(angle_compensate_nodes,
                               0,
                               angle_compensate_nodes_count * sizeof(rplidar_response_measurement_node_t));

                        for(int i = 0; i < count; ++i)
                        {
                            if(nodes[i].distance_q2 != 0)
                            {
                                float angle =
                                    ((nodes[i].angle_q6_checkbit >> RPLIDAR_RESP_MEASUREMENT_ANGLE_SHIFT) / 64.0f);
                                int angle_value = (int) (angle * angle_compensate_multiple);
                                if((angle_value - angle_compensate_offset) < 0)
                                {
                                    angle_compensate_offset = angle_value;
                                }
                                for(int j = 0; j < angle_compensate_multiple; ++j)
                                {
                                    angle_compensate_nodes[angle_value - angle_compensate_offset + j] = nodes[i];
                                }
                            }
                        }

                        std::vector<LaserScan> tmpScans;
                        tmpScans.resize(count);
                        for(int i = 0; i < count; ++i)
                        {
                            // LaserScan(const inch_t& distance, const degree_t& angle);

                            tmpScans[i] = LaserScan(units::length::inch_t(
                                        units::length::millimeter_t(
                                        angle_compensate_nodes[i].distance_q2 / 4.0)), 
                                    units::angle::degree_t((
                                        angle_compensate_nodes[i].angle_q6_checkbit >> RPLIDAR_RESP_MEASUREMENT_ANGLE_SHIFT) / 64.0f));
                        }
                        auto comp = [](LaserScan a, LaserScan b) {
                            return a.angle().value() < b.angle().value();
                        };
                        if (i == 0)
                        {
                            std::move(tmpScans.begin(), tmpScans.end(), scan1.begin());
                            std::sort(scan1.begin(), scan1.end(), comp);
                        }
                        else
                        {
                            std::move(tmpScans.begin(), tmpScans.end(), scan2.begin());
                            std::sort(scan2.begin(), scan2.end(), comp);
                        }
                    }
                }
            }
            std::vector<LaserScan> comboScan = interpolateScans(scan1, scan2);
            m_mutex.lock();
            std::move(comboScan.begin(), comboScan.end(), m_last_scan.begin());
            std::chrono::duration<double, std::milli> scan_time = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch());
            m_scan_timestamp = scan_time.count();
            m_mutex.unlock();
        }
        delete nodes;
    }

    std::vector<LaserScan> RpLidar::interpolateScans(std::vector<LaserScan>& scan1,
            std::vector<LaserScan>& scan2)
    {
        auto comp = [](LaserScan a, LaserScan b) {
            return a.angle().value() < b.angle().value();
        };
        std::vector<LaserScan> combined;
        combined.resize(scan1.size() + scan2.size());
        std::merge(scan1.begin(), scan1.end(), scan2.begin(), scan2.end(),
                combined.begin(), comp);
        unsigned int start_ind = 0;
        for (LaserScan ls : combined)
        {
            if (ls.distance().value() != 0.0 || ls.angle().value() != 0.0)
            {
                break;
            }
            start_ind++;
        }
        if (start_ind == combined.size())
        {
            start_ind = 0;
        }
        std::vector<LaserScan> purged(combined.begin() + start_ind, combined.end());
        std::vector<LaserScan> final_scan;
        auto comp1 = [](double val, LaserScan b) {
            return val < b.angle().value();
        };
        for (unsigned int i = 0; i < 360; i++)
        {
            auto it = std::upper_bound(purged.begin(), purged.end(), (double) i, comp1);
            inch_t dist1, dist2;
            if (it == purged.end() || it == purged.begin())
            {
                dist1 = purged[purged.size()-1].distance();
                dist2 = purged[0].distance();
            }
            else
            {
                dist1 = it->distance();
                dist2 = (it-1)->distance();
            }
            inch_t new_dist = inch_t((dist1.value() + dist2.value()) / 2.0);
            final_scan.push_back(LaserScan(new_dist, degree_t((double) i)));
        }
        return final_scan;
    }
}  // namespace rip
