#ifndef RP_LIDAR_HPP
#define RP_LIDAR_HPP

// Global

// External
#include <rplidar.h>

#include <nlohmann/json.hpp>

// Local
#include <rip/dab/lidar.hpp>

namespace rip::components
{
    NEW_RIP_EX(DriverException)
    NEW_RIP_EX(ConnectionException)
    NEW_RIP_EX(ConfigException)

    /**
     * @class RpLidar
     * @brief A wrapper for the RPLidar library
     */
    class RpLidar : public rip::Lidar
    {
        using RPlidarDriver = rp::standalone::rplidar::RPlidarDriver;

   public:
        /**
         * @brief Constructor
         *
         * @param config The configuration for the lidar
         */
        RpLidar(const nlohmann::json& config);

        RpLidar(int scan_size, std::string serial_port="/dev/ttyUSB0", int baud_rate=115200);

        /**
         * @brief Destructor
         */
        ~RpLidar();

        /**
         * @brief Start the motor and a background thread that
         * receives the scans
         */
        virtual void start() override;

        /**
         * @brief Stops the motor and the background thread that
         * receives the scans
         */
        virtual void stop() override;

        virtual std::vector<std::string> diagnosticCommands() const override {}

    protected:
        /**
         * @brief The function for the background thread which receives
         * information from the lidar
         */
        virtual void run() override;

        std::vector<LaserScan> interpolateScans(std::vector<LaserScan> &scan1, 
                std::vector<LaserScan>& scan2);

    private:
        RPlidarDriver* m_driver;

        std::string m_serial_port;
        int m_baud_rate;
        int m_scan_size;
    };
}  // namespace components

#endif  // RP_LIDAR_HPP
